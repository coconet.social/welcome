# Welcome — read this first

Welcome to Coconet.social Gitlab space! We're using this space to share notes, how-tos, collaborate on ideas and tools to defend digital rights. Before you proceed, to ensure an inclusive, safe, collaborative environment, all members must read and abide by [**Coconet participant guidelines**](https://coconet.social/participant-guidelines/). 

Below you will find some guides to help navigate our online platforms:
* [**Mattermost guide**](https://gitlab.com/coconet.social/welcome/-/wikis/mattermost)
* [**Big Blue Button guide**](https://gitlab.com/coconet.social/welcome/-/wikis/Big-Blue-Button) to use Coco Lounge

## Meeting notes

You can find our [Coconet meetup notes (from October 2020 onwards) in this folder](https://gitlab.com/coconet.social/notes). The folder is only accessible to Coconet members — please [sign up to Gitlab](https://gitlab.com/users/sign_up) and send your Gitlab username to Dianne (dianne@engagemedia.org) or kat (kat@engagemedia.org).

Earlier meetup notes (before October 2020) are hosted in [Collab.Coconet.Social](http://collab.coconet.social/).


